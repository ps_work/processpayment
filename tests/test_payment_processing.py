import unittest
import json
from modules.payment_processing import ProcessPayment


class TestPaymentProcessing(unittest.TestCase):

    def test_cheap_payment(self):
        # assume amount is in between 0-20
        process = ProcessPayment()
        result, status = process.cheap_payment_process()
        result = json.loads(result)

        # assert
        self.assertEqual(result['cheap_payment'], 'success', 'Cheap payment test pass')

    def test_expensive_payment(self):
        # assume amount is in between 21-500
        process = ProcessPayment()
        result, status = process.expensive_payment_process()
        result = json.loads(result)

        # assert
        if 'expensive_payment' in result.keys():
            self.assertEqual(result['expensive_payment'], 'success', 'Expensive payment test pass')
        else:
            self.assertEqual(result['cheap_payment'], 'success', 'Cheap payment test pass')

    def test_premimum_payment(self):
        # assume amount is above 500
        process = ProcessPayment()
        result, status = process.premium_payment_process()
        result = json.loads(result)

        # assert
        self.assertEqual(result['premium_payment'], 'success', 'Premium payment test pass')


if __name__ == '__main__':
    obj = TestPaymentProcessing()

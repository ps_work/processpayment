import unittest
from modules.check_validation import Validate


class TestValidator(unittest.TestCase):

    def test_card_no_with_only_integer_and_limited_length(self):
        # Assume
        card_no = '1234567890123456'
        self.validator = Validate()
        # Action
        result = self.validator.validate_credit_card_no(card_no)
        # Assert
        self.assertTrue(result)

    def test_card_no_with_only_integer_and_limited_length_false(self):
        # Assume
        card_no = '123456789012ad3456'
        # Assert
        self.assertRaises(Exception, self.validator.validate_credit_card_no, card_no)

    def test_card_holder_name_with_only_string_with_limited_length(self):
        # Assume
        card_holder_name = 'John'
        # Action
        result = self.validator.validate_card_holder_name(card_holder_name)
        # Assert
        self.assertTrue(result)

    def test_card_holder_name_with_only_string_with_limited_length_false(self):
        # Assume
        card_holder_name = 'John gfjhlnlkn nmbkjnljghj bkhb'

        # Assert
        self.assertRaises(Exception, self.validator.validate_card_holder_name, card_holder_name)

    def test_card_expire_date_should_not_be_in_past(self):
        # Assume
        exp_date = '25/12'

        # Action
        result = self.validator.validate_exp_date(exp_date)
        # Assert
        self.assertTrue(result)

    def test_card_expire_date_should_not_be_in_past_false(self):
        # Assume
        exp_date = '2501/12'

        # Assert
        self.assertRaises(Exception, self.validator.validate_exp_date, exp_date)

    def test_valid_security_code(self):
        # Assume
        code = 'abc'

        # Action
        result = self.validator.validate_security_code(code)
        # Assert
        self.assertTrue(result)

    def test_valid_security_code_false(self):
        # Assume
        code = '646565'

        # Assert
        self.assertRaises(Exception, self.validator.validate_security_code, code)

    def test_valid_amount_should_be_positive(self):
        # Assume
        amount = 123

        # Action
        result = self.validator.validate_positive_amount(amount)
        # Assert
        self.assertTrue(result)

    def test_valid_amount_should_be_positive_false(self):
        # Assume
        amount = -7.5
        # Assert
        self.assertRaises(Exception, self.validator.validate_positive_amount, amount)












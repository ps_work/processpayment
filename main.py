from flask import Flask
from flask_restplus import Api, Resource, reqparse
from raven.contrib.flask import Sentry
from modules.check_validation import Validate
from modules.payment_processing import ProcessPayment

app = Flask(__name__)
api = Api(app, title='Payment Gateway')

search = api.namespace('Payment')
sentry = Sentry(app)

card_details_parser = reqparse.RequestParser()
card_details_parser.add_argument('CreditCardNumber', type=int, required=True, help='7894 4561 1234 456')
card_details_parser.add_argument('CardHolder', type=str, required=True, help='Mr/Mrs')
card_details_parser.add_argument('ExpirationDate', type=str, required=True, help='YY/MM')
card_details_parser.add_argument('SecurityCode', type=str, required=False, help='000')
card_details_parser.add_argument('Amount', type=float, required=True, help='Amount')


@search.route('/payment_gateway')
class PaymentGateway(Resource):
    @search.expect(card_details_parser, validate=True)
    def get(self):
        args = card_details_parser.parse_args()
        # print(args)
        try:
            obj = Validate()
            obj.validate_request(args)
        except:
            response = 'Bad request'
            status = 400
            return response, status

        obj = ProcessPayment()
        if args['Amount'] <= 20:
            response, status = obj.cheap_payment_process()
            return response, status
        elif 20 < args['Amount'] <= 500:
            response, status = obj.expensive_payment_process()
            return response, status
        elif args['Amount'] > 500:
            response, status = obj.premium_payment_process()
            return response, status


if __name__ == '__main__':
    app.run(debug=True, use_reloader=False)

from modules.cheapPayment import CheapPaymentGateway
from modules.expensivePayment import ExpensivePaymentGateway
from modules.premiumPayment import PremiumPaymentGateway


class ProcessPayment:
    def __init__(self):
        self.count = 0

    def cheap_payment_process(self):
        try:
            obj_cheap = CheapPaymentGateway()
            response = obj_cheap.cheap_payment()
            status = 200
            return response, status
        except Exception as e:
            print(e)
            response = 'internal server error'
            status = 500
            return response, status

    def expensive_payment_process(self):
        try:
            try:
                obj_exp = ExpensivePaymentGateway()
                response = obj_exp.expensive_payment()
                status = 200
                return response, status
            except:
                obj = CheapPaymentGateway()
                response = obj.cheap_payment()
                status = 200
                return response, status
        except Exception as e:
            print(e)
            response = 'internal server error'
            status = 500
            return response, status

    def premium_payment_process(self):
        obj_pre = PremiumPaymentGateway()
        try:
            count = 0
            for retry in range(0, 3):
                try:
                    count += 1
                    response = obj_pre.premimum_payment()
                    status = 200
                    return response, status
                except:
                    if count == 3:
                        raise Exception
                    else:
                        pass
        except:
            response = 'internal server error'
            status = 500
            return response, status


if __name__ == '__main__':
    obj = ProcessPayment()




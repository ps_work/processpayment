from datetime import datetime


class Validate:

    def validate_request(self, args):
        print(args)
        self.validate_credit_card_no(args['CreditCardNumber'])
        self.validate_card_holder_name(args['CardHolder'])
        self.validate_exp_date(args['ExpirationDate'])
        self.validate_security_code(args['SecurityCode'])
        self.validate_positive_amount(args['Amount'])

    def validate_credit_card_no(self, card_no):
        print('validate_credit_card_no')
        valid_no = str(card_no).isdigit()
        print(valid_no)
        if len(str(card_no)) in range(13, 20) and valid_no == True:
            return True
        else:
            raise Exception

    def validate_card_holder_name(self, card_holder_name):
        print('validate_card_holder_name')
        if len(card_holder_name) in range(2, 25):
            return True
        else:
            raise Exception

    def validate_exp_date(self, given_date):
        print('validate_exp_date')
        check_given_date = datetime.strptime(given_date, '%y/%m')
        today = datetime.today()
        date_tup = today.year, today.month
        current_datetime = '{year}/{month}'.format(year=str(date_tup[0])[-2:], month=date_tup[1])
        if current_datetime > given_date:
            raise Exception
        else:
            return True

    def validate_security_code(self, security_code):
        print('validate_security_code')
        if len(security_code) == 3:
            return True
        else:
            raise Exception

    def validate_positive_amount(self, amount):
        print('valid amount')
        if amount <= 0:
            raise Exception
        else:
            return True


if __name__ == '__main__':
    obj = Validate()
